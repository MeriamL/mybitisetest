package com.renault.gitlabbitrisetest

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import org.hamcrest.CoreMatchers.not
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test


/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
//@RunWith(AndroidJUnit4::class)
@LargeTest
class ExampleInstrumentedTest {

    @get:Rule
    var activityScenarioRule = ActivityScenarioRule(MainActivity::class.java)


    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("com.renault.gitlabbitrisetest", appContext.packageName)
    }

    @Test
    fun test1(){
        onView(withId(R.id.textView)).check(matches(withText("Hello World!")))
    }

    @Test
    fun test2(){
        onView(withId(R.id.button)).perform(click())
        onView(withId(R.id.textView)).check(matches(withText("Hello World!")))
        onView(withId(R.id.textView)).check(matches(not(isDisplayed())))
    }

    @Test
    fun test3(){
        onView(withId(R.id.button)).perform(click())
        onView(withId(R.id.textView)).check(matches(withText("Hello World!")))
        onView(withId(R.id.textView)).check(matches(not(isDisplayed())))
        onView(withId(R.id.button)).perform(click())
        onView(withId(R.id.textView)).check(matches(isDisplayed()))
    }

//
//    @Test
//    fun changeText_sameActivity() {
//        // Type text and then press the button.
////        onView(withId(R.id.editTextUserInput))
////            .perform(typeText(STRING_TO_BE_TYPED), closeSoftKeyboard())
////        onView(withId(R.id.changeTextBt)).perform(click())
//
//        // Check that the text was changed.
//        onView(withId(R.id.textView)).check(matches(withText("Hello world")))
//    }
}
